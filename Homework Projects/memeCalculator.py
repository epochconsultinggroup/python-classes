# Hearthstone Meme Calculator
# Made by Daniel Stefek
print("Let's see if your Hearthstone deck is a meme or not!")

def memedeck():
    classList = ["hunter", "rogue", "paladin", "mage", "druid", "warrior", "warlock", "demon hunter"]
    deckType = ['control', 'mill', 'combo']
    memeScore = 0
    input()

    while True:
        whatClass = input("\nWhich class are you playing?: ").casefold()

        if whatClass == "priest":
            print("Yikes")
            memeScore += 1
            break

        elif whatClass in classList:
            print("Okay okay cool cool cool.")
            break

        else:
            print("That's not a class silly, try again")
            continue

    while True:
        whatType = input("What is your deck's archetype?: ").casefold()

        if whatType in deckType:
            print("Oh dear.")
            memeScore += 1
            break

        elif whatType in ("aggro", "midrange"):
            print("Not bad at all.")
            break

        else:
            print("Is that even a thing?")
            continue


    while True:
        whatTurn = int(input("By what turn should you win?: "))

        if whatTurn <= 9:
            print("Could be worse.")
            memeScore += 1
            break

        else:
            print("But you don't think that might be a bit slow?")
            continue

    validInput = False
    while(validInput == False):
        playOne = input("Are you playing Blood of The Ancient One?: ").casefold()

        if playOne == "yes":
            print("...Fuck")
            memeScore += 2
            validInput = True

        elif playOne == "no":
            print("Well That is good at least.")
            validInput = True
        else:
            print("ANSWER ME DAMN YOU!")
            continue

    return memeScore


def calculateMemeScore(memeScore):
    if memeScore >= 3:
        print('This is a meme build')
    elif memeScore <= 2 and memeScore > 0:
        print('Kinda memey')
    else:
        print('Not a meme')


calculateMemeScore(memedeck())


