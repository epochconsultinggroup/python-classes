from Creature import Creature

card1 = Creature('Rat King', 'The king of all rats', 2, 5, 10)
card2 = Creature('Rat Peon', 'A lowly rat peon', 1, 2, 5)

card1.showStats()
card2.showStats()

card1.attack_card(card2)

