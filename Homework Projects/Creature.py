from HearthstoneCard import HearthstoneCard


class Creature(HearthstoneCard):
    def __init__(self, name, description, cost, attackValue, health):
        self.name = name
        self.description = description
        self.cardType = 'Creature'
        self.cost = cost
        self.attackValue = attackValue
        self.health = health

    def attack_card(self, opposingCard):
        opposingCard.health -= self.attackValue
        print('The opposing card now has', opposingCard.health, 'health')
        if opposingCard.health <= 0:
            print('You have killed the', opposingCard.name, 'card')

