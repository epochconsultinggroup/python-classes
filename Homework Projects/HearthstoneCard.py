# This is a object of type HearthstoneCard
class HearthstoneCard:

    def __init__(self, name, description, cardType, cost):
        self.name = name
        self.description = description
        self.cardType = cardType
        self.cost = cost

    def showStats(self):
        print('This is a', self.name, 'card')
        print('This is a', self.cardType, 'type of card.')
        print('The card has a cost of', self.cost)
